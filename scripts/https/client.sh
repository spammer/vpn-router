[ -n "$1" ] && i=$1 || i=1
thisDir=`dirname $0`
keysDir=`realpath $thisDir/../../keys`
clientDir=$keysDir/client$i
mkdir -p $clientDir
openssl genrsa -out $clientDir/client-key.pem 4096
export CLIENT_ID=$i
openssl req -new -config $thisDir/client.cnf -key $clientDir/client-key.pem -out $clientDir/client-csr.pem
openssl x509 -req -extfile $thisDir/client.cnf -days 999 -passin "pass:5Botac!SHt@" -CAcreateserial \
  -in $clientDir/client-csr.pem -CA $keysDir/ca-crt.pem -CAkey $keysDir/ca-key.pem -out $clientDir/client-crt.pem
openssl verify -CAfile $keysDir/ca-crt.pem $clientDir/client-crt.pem
