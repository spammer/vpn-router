thisDir=`dirname $0`
keysDir=`realpath $thisDir/../../keys`
openssl req -new -x509 -days 9999 -config $thisDir/ca.cnf -keyout $keysDir/ca-key.pem -out $keysDir/ca-crt.pem
