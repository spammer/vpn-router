thisDir=`dirname $0`
keysDir=`realpath $thisDir/../../keys`
openssl genrsa -out $keysDir/server-key.pem 4096
#export CN=94.100.96.66
export CN=localhost
openssl req -new -config $thisDir/server.cnf -key $keysDir/server-key.pem -out $keysDir/server-csr.pem
openssl x509 -req -extfile $thisDir/server.cnf -days 999 -passin "pass:5Botac!SHt@" \
  -in $keysDir/server-csr.pem -CA $keysDir/ca-crt.pem -CAkey $keysDir/ca-key.pem -CAcreateserial -out $keysDir/server-crt.pem
#openssl dhparam -out $keysDir/dh2048.pem 2048
# openssl x509 -text -noout -in $keysDir/server-crt.pem | grep CN
