const _ = require('lodash');
const bodyParser = require('body-parser');
const express = require('express');
const fs = require('fs');
const path = require('path');

const app = express();

app.use(bodyParser.json({ limit: '2mb' }));
app.use(bodyParser.urlencoded({ limit: '2mb', extended: true }));
app.use(express.static(path.join(__dirname, 'test'), { maxAge: 31557600000 }));

// const listener1 = app.listen(9001, () => {
//   console.log(`Listening on port ${listener1.address().port}`);
// });
// const listener2 = app.listen(9002, () => {
//   console.log(`Listening on port ${listener2.address().port}`);
// });

const har = JSON.parse(fs.readFileSync('tmp/beautifull.life.har', 'utf8'));
const results = har.log.entries
  .map((e) => {
    const h = _.find(e.request.headers, ['name', 'Referer']);
    if (!h) return {};
    return {
      url: e.request.url,
      referer: h.value
    };
  }).filter(o => /localhost/.test(o.referer));

fs.writeFileSync('referers', JSON.stringify(results, null, 2));
