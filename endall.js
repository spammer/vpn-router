const fs = require('fs');
const rp = require('request-promise');

(async () => {
  const body = await rp({
    url: 'https://94.100.96.66:8789/endall',
    // url: 'https://localhost:8789/ping',
    agentOptions: {
      ca: fs.readFileSync(`${__dirname}/keys/ca-crt.pem`),
      cert: fs.readFileSync(`${__dirname}/keys/client1/client-crt.pem`),
      key: fs.readFileSync(`${__dirname}/keys/client1/client-key.pem`),
      rejectUnauthorized: false
    }
  });
  console.log(body);
})();
