const fs = require('fs');
// const util = require('util');
const rp = require('request-promise');
// const aRequest = util.promisify(require('request'));

(async () => {
  try {
    const body = await rp({
      // method: 'POST',
      url: 'https://localhost:8789/ping',
      agentOptions: {
        ca: fs.readFileSync(`${__dirname}/keys/ca-crt.pem`),
        cert: fs.readFileSync(`${__dirname}/keys/client1/client-crt.pem`),
        key: fs.readFileSync(`${__dirname}/keys/client1/client-key.pem`),
        rejectUnauthorized: false
      },
      // json: {
      //   lane: 1,
      //   zip: fs.readFileSync('tmp/test.zip', 'base64')
      // }
    });
    console.log('body', body);
  } catch (ex) {
    console.log('ex', ex.message);
  }
})();
