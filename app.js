// cSpell:ignore novpn
const bodyParser = require('body-parser');
const { execSync, spawn, spawnSync } = require('child_process');
const express = require('express');
const fs = require('fs-extra');
const https = require('https');
const util = require('util');
const glob = util.promisify(require('glob'));
const logger = require('morgan');
const Zip = require('adm-zip');

const app = express();
const compact = r => `<pre>${r
  .output.concat(r.error)
  .map(b => (b || '').toString('utf8'))
  .join('\n')}</pre>`;
const credentials = {
  key: fs.readFileSync('keys/server-key.pem'),
  cert: fs.readFileSync('keys/server-crt.pem'),
  ca: fs.readFileSync('keys/ca-crt.pem'),
  securityOptions: 'SSL_OP_NO_SSLv3',
  requestCert: true,
  rejectUnauthorized: false
};
const mode = app.get('env');
const sleep = util.promisify(setTimeout);
const stopDocker = lane => spawnSync('docker', ['stop', `ssh-vpn${lane}`]);
const waitLimit = 40000;
let logType = '';

app.use((req, res, next) => {
  if (!res.connection.authorized || res.connection.authorizationError) {
    res.sendStatus(403);
    return;
  }
  // req.socket.getPeerCertificate().subject.CN
  next();
});
app.use(bodyParser.json({ limit: '2mb' }));
app.use(bodyParser.urlencoded({ limit: '2mb', extended: true }));
if (mode === 'development') {
  // app.use(errorHandler());
  logType = 'dev';
} else if (mode === 'production') {
  logType = 'combined';
}
if (logType) {
  // convert log from UTC to local time
  logger.token('date', () => {
    const p = new Date().toString().replace(/[A-Z]{3}\+/, '+').split(/ /);

    return `${p[2]}/${p[1]}/${p[3]}:${p[4]} ${p[5]}`;
  });
  app.use(logger(logType));
}
app.post('/start', async (req, res) => {
  const cId = req.body.lane;
  const vpnDir = `vpn/${cId}`;
  const dataZip = `${vpnDir}/data.zip`;
  const container = `ssh-vpn${cId}`;
  const dockerArgs = [
    'run', '-d', '--rm', '--name', container,
    '--network', `net${cId}`, '--device', '/dev/net/tun',
    '--cap-add', 'NET_ADMIN', '--sysctl', 'net.ipv6.conf.all.disable_ipv6=0',
    '-v', `${__dirname}/vpn/${cId}:/vpn`, '-p', `2200${cId}:22`,
    '--log-driver', 'fluentd', '--log-opt', `tag=${cId}`
  ];
  let ovpn = '';

  fs.ensureDirSync(vpnDir);
  fs.emptyDirSync(vpnDir);
  fs.writeFileSync(dataZip, Buffer.from(req.body.zip, 'base64'));
  new Zip(dataZip).extractAllTo(vpnDir);
  if (req.body.novpn) {
    dockerArgs.push('-t', '--entrypoint', '/usr/sbin/sshd');
  }
  dockerArgs.push('ssh-vpn');
  if (req.body.novpn) {
    dockerArgs.push('-D');
  } else {
    ovpn = (await glob('*.ovpn', { cwd: vpnDir }))[0];
    dockerArgs.push(ovpn);
  }
  if (execSync(`docker ps -a | grep ${container} | cut -d " " -f1`).length) {
    console.log(`Stopping ${container} ...`);
    stopDocker(req.body.lane);
  }

  const result = spawnSync('docker', dockerArgs);
  const stepDelay = 2000;
  const output1 = compact(result);
  const logFile = `${vpnDir}/fluentd.log`;
  let output2, secret, until = waitLimit, errorTxt;

  if (result.status !== 0) {
    res.status(500).send(output1);
    return;
  }
  while (until > 0 && !req.body.novpn) {
    until -= stepDelay;
    await sleep(stepDelay);
    try {
      if (fs.existsSync(logFile)) output2 = fs.readFileSync(logFile, 'utf8');
    } catch (ex) { console.log(ex); }
    if (/AUTH_FAILED/.test(output2)) {
      secret = fs.readFileSync(`${vpnDir}/login`, 'utf8').split('\n');
      res.status(500).send(`AUTH_FAILED: ${ovpn} => ${secret[0]} / ${secret[1]}`);
      return;
    }
    if (/TLS handshake failed/.test(output2)) {
      errorTxt = 'TLS key negotiation failed';
      break;
    }
    if (/Initialization Sequence Completed/.test(output2)) {
      try {
        execSync(`docker exec ${container} ping -c 1 -w 3 8.8.4.4 > /dev/null`);
        res.sendStatus(200);
      } catch (ex) {
        res.status(500).send('No internet connection within the VPN');
      }
      return;
    }
  }
  if (until < 1) {
    errorTxt = 'could not start in time';
  }

  const ip = output2.match(/](\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}(:\d+))/);

  stopDocker(req.body.lane);
  res.status(500).send(`${errorTxt}: ${ovpn} ${ip ? ip[1] : ''}`);
});
app.get('/status', (req, res) => {
  const result = spawnSync('docker', ['logs', `ssh-vpn${req.query.lane}`]);
  res.status(!result.status ? 200 : 500).send(compact(result));
});
app.get('/end', (req, res) => {
  const result = stopDocker(req.query.lane);
  res.status(result.status !== 0 ? 500 : 200).send(compact(result));
});
app.get('/endall', (req, res) => {
  const containers = spawnSync('sh', [
    '-c', 'docker ps -a | egrep -v "(ssh-vpn0|fluentd)" | cut -d " " -f1'
  ]).output[1].toString('utf8').split('\n').slice(1).filter(c => c);
  // console.log(containers);
  spawn('docker', ['stop', ...containers]);
  res.sendStatus(200);
});

app.get('/ping', async (req, res) => {
  await sleep(15000);
  res.sendStatus(200);
});

const listener = https.createServer(credentials, app).listen(8789, () => {
  if (mode === 'test') return;
  console.log(`Listening for VPN configs on port ${listener.address().port}`);
});
